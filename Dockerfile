FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY Sapphirite.Api.Accounts/Sapphirite.Api.Accounts.csproj ./Sapphirite.Api.Accounts/
RUN dotnet restore ./Sapphirite.Api.Accounts/

# Copy everything else and build
COPY ./Sapphirite.Api.Accounts ./Sapphirite.Api.Accounts/
RUN dotnet publish -c Release -o out ./Sapphirite.Api.Accounts/

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/Sapphirite.Api.Accounts/out .
ENTRYPOINT ["dotnet", "Sapphirite.Api.Accounts.dll", "--urls=http://0.0.0.0:5000"]