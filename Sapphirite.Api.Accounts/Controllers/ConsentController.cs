
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sapphirite.Api.Accounts.Models;
using Sapphirite.Api.Accounts.Services;

namespace Sapphirite.Api.Accounts.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsentController : ControllerBase
    {
        private readonly HydraAdminClient _hydra;

        public ConsentController(HydraAdminClient hydra)
        {
            _hydra = hydra;
        }

        [HttpPut("request/{challenge}/start")]
        public async Task<ActionResult<CompletedConsentRequestStart>> PutRequestStart(string challenge)
        {
            var hydraRequest = await _hydra.GetConsentRequest(challenge);

            if (hydraRequest.Skip) {
                var hydraCompleted = await _hydra.AcceptConsentRequest(
                    challenge,
                    new HydraAcceptConsentRequest
                    {
                        GrantScope = hydraRequest.RequestedScope
                    }
                );

                return new CompletedConsentRequestStart
                {
                    RedirectTo = hydraCompleted.RedirectTo
                };
            }

            return new CompletedConsentRequestStart
            {
                RequestedScope = hydraRequest.RequestedScope,
                ClientName = hydraRequest.Client.ClientName,
                ClientOwner = hydraRequest.Client.Owner,
            };
        }

        [HttpPut("request/{challenge}/accept")]
        public async Task<ActionResult<CompletedConcentRequestAction>> PutRequestAccept(
            string challenge
        ) {
            var hydraRequest = await _hydra.GetConsentRequest(challenge);

            // The route tells us that the user wants to accept, so do that
            var hydraCompleted = await _hydra.AcceptConsentRequest(
                challenge,
                new HydraAcceptConsentRequest
                {
                    GrantScope = hydraRequest.RequestedScope,
                    Remember = true,
                    RememberFor = 86400
                }
            );
            return new CompletedConcentRequestAction
            {
                RedirectTo = hydraCompleted.RedirectTo
            };
        }

        [HttpPut("request/{challenge}/reject")]
        public async Task<ActionResult<CompletedConcentRequestAction>> PutRequestReject(
            string challenge
        ) {
            var hydraRequest = await _hydra.GetConsentRequest(challenge);

            // The route tells us that the user wants to reject, so do that
            var hydraCompleted = await _hydra.RejectConsentRequest(
                challenge,
                new HydraRejectRequest
                {
                    Error = "user_rejected",
                    ErrorDescription = "The user rejected the request.",
                    ErrorHint = ""
                }
            );
            return new CompletedConcentRequestAction
            {
                RedirectTo = hydraCompleted.RedirectTo
            };
        }
    }
}