using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Sapphirite.Api.Accounts.Models;
using Sapphirite.Api.Accounts.Services;

namespace Sapphirite.Api.Accounts.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly HydraAdminClient _hydra;

        public LoginController(HydraAdminClient hydra)
        {
            _hydra = hydra;
        }

        /// Starts the request, checking if it needs to be skipped, and if so skipping it. If not,
        /// returing data about the request.
        [HttpPut("request/{challenge}/start")]
        public async Task<ActionResult<CompletedLoginRequestStart>> PutRequestStart(
            string challenge)
        {
            var hydraRequest = await _hydra.GetLoginRequest(challenge);

            // Start by checking if we need to skip, if so just return a redirect
            if (hydraRequest.Skip)
            {
                var hydraCompleted = await _hydra.AcceptLoginRequest(
                    challenge,
                    new HydraAcceptLoginRequest
                    {
                        Subject = hydraRequest.Subject,
                    }
                );
                return new CompletedLoginRequestStart
                {
                    RedirectTo = hydraCompleted.RedirectTo,
                };
            }

            return new CompletedLoginRequestStart
            {
                ClientName = hydraRequest.Client.ClientName,
                ClientOwner = hydraRequest.Client.Owner,
            };
        }

        [HttpPut("request/{challenge}/attempt")]
        public async Task<ActionResult<CompletedLoginRequestAttempt>> PutRequestAttempt(
            string challenge,
            [FromBody] LoginRequestAttempt request)
        {
            // Temporary test validation
            if (request.Email == "user@example.com" && request.Password == "password")
            {
                var hydraCompleted = await _hydra.AcceptLoginRequest(
                    challenge,
                    new HydraAcceptLoginRequest
                    {
                        Subject = Guid.NewGuid().ToString(),
                        Remember = request.Remember,
                        RememberFor = request.Remember ? (int?)86400 : null,
                    }
                );
                return new CompletedLoginRequestAttempt
                {
                    RedirectTo = hydraCompleted.RedirectTo
                };
            }
            else
            {
                // The /reject route is for fatal errors, if we just have an invalid password send
                // it back to the frontend.
                return BadRequest(new ApiAccountsError
                {
                    Error = "Email and Password do not match an account."
                });
            }
        }
    }
}