namespace Sapphirite.Api.Accounts.Models
{
    public class ApiAccountsError
    {
        public string Error { get; set; }
    }
}