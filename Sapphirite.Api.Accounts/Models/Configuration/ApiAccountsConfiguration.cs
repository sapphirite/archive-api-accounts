using System.ComponentModel.DataAnnotations;

namespace Sapphirite.Api.Accounts.Models.Configuration
{
    public class ApiAccountsConfiguration
    {
        [Required]
        public string HydraAdminApi { get; set; }
    }
}