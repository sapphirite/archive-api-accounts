namespace Sapphirite.Api.Accounts.Models
{
    public class CompletedConsentRequestStart
    {
        public string RedirectTo { get; set; }

        public string[] RequestedScope { get; set; }

        public string ClientName { get; set; }

        public string ClientOwner { get; set; }
    }

    public class CompletedConcentRequestAction
    {
        public string RedirectTo { get; set; }
    }
}