namespace Sapphirite.Api.Accounts.Models
{
    public class CompletedLoginRequestStart
    {
        public string RedirectTo { get; set; }

        public string ClientName { get; set; }

        public string ClientOwner { get; set; }
    }

    public class LoginRequestAttempt
    {
        public string Email { get; set; }

        public string Password { get; set; }
    
        public bool Remember { get; set; }
    }

    public class CompletedLoginRequestAttempt
    {
        public string RedirectTo { get; set; }
    }
}