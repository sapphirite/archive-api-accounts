using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Sapphirite.Api.Accounts.Models;
using Sapphirite.Api.Accounts.Models.Configuration;

namespace Sapphirite.Api.Accounts.Services
{
    public class HydraAdminClient
    {
        private HttpClient _client;

        public HydraAdminClient(IOptions<ApiAccountsConfiguration> config, HttpClient client)
        {
            client.BaseAddress = new Uri(config.Value.HydraAdminApi);

            _client = client;
        }

        public async Task<HydraConsentRequest> GetConsentRequest(string challenge)
        {
            var response = await _client.GetAsync("/oauth2/auth/requests/consent/" + challenge);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<HydraConsentRequest>();
        }

        public async Task<HydraCompletedRequest> AcceptConsentRequest(
            string challenge,
            HydraAcceptConsentRequest acceptConsentRequest)
        {
            var response = await _client.PutAsJsonAsync(
                "/oauth2/auth/requests/consent/" + challenge + "/accept",
                acceptConsentRequest
            );
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<HydraCompletedRequest>();
        }

        public async Task<HydraCompletedRequest> RejectConsentRequest(
            string challenge,
            HydraRejectRequest rejectRequest)
        {
            var response = await _client.PutAsJsonAsync(
                "/oauth2/auth/requests/consent/" + challenge + "/reject",
                rejectRequest
            );
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<HydraCompletedRequest>();
        }

        public async Task<HydraLoginRequest> GetLoginRequest(string challenge)
        {
            var response = await _client.GetAsync("/oauth2/auth/requests/login/" + challenge);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<HydraLoginRequest>();
        }

        public async Task<HydraCompletedRequest> AcceptLoginRequest(
            string challenge,
            HydraAcceptLoginRequest acceptLoginRequest)
        {
            var response = await _client.PutAsJsonAsync(
                "/oauth2/auth/requests/login/" + challenge + "/accept",
                acceptLoginRequest
            );
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<HydraCompletedRequest>();
        }

        public async Task<HydraCompletedRequest> RejectLoginRequest(
            string challenge,
            HydraRejectRequest rejectRequest)
        {
            var response = await _client.PutAsJsonAsync(
                "/oauth2/auth/requests/login/" + challenge + "/reject",
                rejectRequest
            );
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<HydraCompletedRequest>();
        }
    }

    public class HydraOAuth2Client
    {
        // Incomplete

        [JsonProperty("client_name")]
        public string ClientName { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }
    }

    public class HydraConsentRequest
    {
        // Incomplete

        [JsonProperty("client")]
        public HydraOAuth2Client Client { get; set; }

        [JsonProperty("skip")]
        public bool Skip { get; set; }

        /// Subject is the user ID of the end-user that authenticated. Now, that end user needs to
        /// grant or deny the scope requested by the OAuth 2.0 client.
        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("requested_scope")]
        public string[] RequestedScope { get; set; }
    }

    public class HydraAcceptConsentRequest
    {
        // Incomplete

        [JsonProperty("grant_scope")]
        public string[] GrantScope { get; set; }

        [JsonProperty("remember")]
        public bool? Remember { get; set; }

        [JsonProperty("remember_for")]
        public long? RememberFor { get; set; }

        [JsonProperty("session")]
        public HydraConsentRequestSession Session { get; set; }
    }

    public class HydraConsentRequestSession
    {
        [JsonProperty("access_token")]
        public dynamic AccessToken { get; set; }

        [JsonProperty("id_token")]
        public dynamic IdToken { get; set; }
    }

    public class HydraLoginRequest
    {
        // Incomplete

        [JsonProperty("challenge")]
        public string Challenge { get; set; }

        [JsonProperty("client")]
        public HydraOAuth2Client Client { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("skip")]
        public bool Skip { get; set; }
    }

    public class HydraAcceptLoginRequest
    {
        /// ACR sets the Authentication AuthorizationContext Class Reference value for this
        /// authentication session. You can use it to express that, for example, a user
        /// authenticated using two factor authentication.
        [JsonProperty("acr")]
        public string Acr { get; set; }

        [JsonProperty("force_subject_identifier")]
        public string ForceSubjectIdentifier { get; set; }

        [JsonProperty("remember")]
        public bool? Remember { get; set; }

        [JsonProperty("remember_for")]
        public long? RememberFor { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }
    }

    public class HydraRejectRequest
    {
        // Incomplete

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }

        [JsonProperty("error_hint")]
        public string ErrorHint { get; set; }
    }

    public class HydraCompletedRequest
    {
        [JsonProperty("redirect_to")]
        public string RedirectTo { get; set; }
    }
}