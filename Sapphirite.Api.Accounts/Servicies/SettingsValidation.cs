using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Sapphirite.Api.Accounts.Models.Configuration;

namespace Sapphirite.Api.Accounts.Services
{
    public class SettingValidation : IStartupFilter
    {
        readonly ApiAccountsConfiguration _configuration;

        public SettingValidation(IOptions<ApiAccountsConfiguration> configuration)
        {
            _configuration = configuration.Value;
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            var context = new ValidationContext(_configuration);
            Validator.ValidateObject(_configuration, context);

            return next;
        }
    }
}