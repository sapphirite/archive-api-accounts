# Sapphirite API Accounts
Accounts and authorization service for Sapphirite.
This service integrates with [ORY Hydra](https://github.com/ory/hydra) to provide OAuth2 endpoints.

## Deployment
This project is intended to be hosted in docker or kubernetes.
The API will listen on port 5000.

### Environment Variables
- ApiAccounts__HydraAdminApi: An URL pointing to hydra's admin API. For example: "http://hydra:4445"

## Development Setup
1. Copy the example "/appsettings.Local.json" file to "/Sapphirite.Api.Accounts/".
2. Run `docker-compose up` to start up development services this service depends on.

### Smoke Testing
You can smoke test the docker container before publishing using the smoke docker-compose file:
```bash
docker-compose -f docker-compose.yml -f docker-compose.smoke.yml up --build
```

### Creating a Client in Hydra
PUSH: http://localhost:4445/clients
```json
{
	"client_id": "devclient",
	"client_name": "Development Client",
	"client_uri": "http://localhost:3000",
	"owner": "Developer",
	"redirect_uris": ["http://localhost:3000/callback"],
	"grant_types": ["authorization_code", "refresh_token"],
	"response_types": ["code"],
	"scope": "openid",
	"token_endpoint_auth_method": "none"
}
```
token_endpoint_auth_method=none makes this a public client

#### To-Dos:
- Automatically register a client for logging into the account system's management control panel.
- Provide a way for clients to register themselves during setup through user permission.

### Getting a Login Challenge
GET: http://localhost:4444/oauth2/auth?client_id=dev-client&response_type=code&state=12345678

## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
